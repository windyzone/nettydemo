package com.example.demo.model;

import java.io.Serializable;
import lombok.Data;

/**
 * @ClassName: Tenant
 * @Author: huangzf
 * @Date: 2018/8/6 15:16
 * @Description:
 */
@Data
public class Tenant implements Serializable {


    private static final long serialVersionUID = -7033707301911915196L;

    private String tenantId;

}
