package com.example.demo.model;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName: Result
 * @Author: huangzf
 * @Date: 2018/8/6 15:57
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result implements Serializable {
    private static final long serialVersionUID = -7033707301911915197L;

    private int code;
    private Object message;
    private String uniId;


}
